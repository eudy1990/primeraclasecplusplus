#include <iostream>
using namespace std;

//Arbol binario de busqueda

struct Nodo{
	int dato;
	Nodo *der;
	Nodo *izq;
};

Nodo *arbol = NULL;

//Funcion para crear nuevo Nodo
Nodo *crearNodo(int n){
	Nodo *nuevo_nodo = new Nodo();
	
	nuevo_nodo->dato = n;
	nuevo_nodo->der = NULL;
	nuevo_nodo->izq = NULL;
	
	return nuevo_nodo;
}


//Funcion para insertar elementos en el arbol
void insertarNodo(Nodo *&arbol,int n){
	
	if(arbol == NULL){//Si el arbol esta vacio
		//Creamos un nuevo Nodo
		Nodo *nuevo_nodo = crearNodo(n);
		//lo asignamos al arbol
		arbol = nuevo_nodo;
	}else{//Si el arbol tiene un nodo o mas 
		int valorRaiz = arbol->dato;//obtenemos el valor de la raiz
		if(n < valorRaiz){//Si el elemento es menor a la raiz, insertamos a la izq
			insertarNodo(arbol->izq,n);
		}else{//Si el nodo es mayor a la raiz, insertamos en der
			insertarNodo(arbol->der,n);
		}
		
		
	}
	
}
void mostrarArbol(Nodo *arbol, int cont){
	if(arbol == NULL){
		return;
	}
	else{
		mostrarArbol(arbol->der,cont+1);
		for(int i=0;i<cont;i++){
			cout<<"   ";
		}
		cout<<arbol->dato<<endl;
		mostrarArbol(arbol->izq,cont+1);
	}
}

int main(int argc, char *argv[]) {
	int dato;
	int contador = 0;
	
	dato = 50;
	insertarNodo(arbol,dato);
	
	dato = 30;
	insertarNodo(arbol,dato);
	
	dato = 55;
	insertarNodo(arbol,dato);
	
	dato = 20;
	insertarNodo(arbol,dato);
	
	dato = 35;
	insertarNodo(arbol,dato);
	
	mostrarArbol(arbol,contador);
	
	//8,3,10,1,6,14,4,7,13
	return 0;
}

