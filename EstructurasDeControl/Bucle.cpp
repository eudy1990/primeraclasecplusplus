#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int numero=2;
	cout <<"==========================================================="<<endl;
	cout<<" Bucle FOR "<<endl;
	cout <<"==========================================================="<<endl;
	for(int i = 0;i<=12; i++){
		int resultado = (numero * i);
		cout<<numero<<" X "<<i<<" = "<< resultado <<endl;
	}
	cout <<"==========================================================="<<endl;
		cout<<" Bucle While "<<endl;
	cout <<"==========================================================="<<endl;
	int i = 0;
	while(i<=12 ){
		
		int resultado = (numero * i);
		cout<<numero<<" X "<<i<<" = "<< resultado <<endl;
		i++;
	}
	cout <<"==========================================================="<<endl;
	cout<<" Bucle DO While "<<endl;
	cout <<"==========================================================="<<endl;
	i = 0;
	do{
		
		int resultado = (numero * i);
		cout<<numero<<" X "<<i<<" = "<< resultado <<endl;
		i++;
	}while(i<=12 );
		
	int numero=2;
	cout <<"==========================================================="<<endl;
	cout<<" Bucle FOR "<<endl;
	cout <<"==========================================================="<<endl;
	for(int i = 0;i<=12; i++){
		int resultado = (numero * i);
		cout<<numero<<" X "<<i<<" = "<< resultado <<endl;
	}
	return 0;
}

