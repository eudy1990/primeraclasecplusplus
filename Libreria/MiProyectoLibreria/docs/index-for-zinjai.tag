<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile doxygen_version="1.9.2" doxygen_gitid="caa4e3de211fbbef2c3adf58a6bd4c86d0eb7cb8">
  <compound kind="file">
    <name>libreria.c</name>
    <path>C:/Users/dj23/Documents/Gitlab/primeraclasecplusplus/Libreria/MiProyectoLibreria/</path>
    <filename>libreria_8c.html</filename>
    <includes id="libreria_8h" name="libreria.h" local="no" imported="no">libreria.h</includes>
    <member kind="function">
      <type>float</type>
      <name>suma</name>
      <anchorfile>libreria_8c.html</anchorfile>
      <anchor>ab59feec5b81b3ecb84981c691804abff</anchor>
      <arglist>(float A, float B)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>resta</name>
      <anchorfile>libreria_8c.html</anchorfile>
      <anchor>a12052d25a22c6ed551d9b95edf4f9b0a</anchor>
      <arglist>(float A, float B)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>multiplicacion</name>
      <anchorfile>libreria_8c.html</anchorfile>
      <anchor>a7768dd53e17864f3487d42acc915ae18</anchor>
      <arglist>(float A, float B)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>libreria.h</name>
    <path>C:/Users/dj23/Documents/Gitlab/primeraclasecplusplus/Libreria/MiProyectoLibreria/</path>
    <filename>libreria_8h.html</filename>
    <includes id="libreria_8c" name="libreria.c" local="no" imported="no">libreria.c</includes>
    <member kind="function">
      <type>float</type>
      <name>suma</name>
      <anchorfile>libreria_8h.html</anchorfile>
      <anchor>ab59feec5b81b3ecb84981c691804abff</anchor>
      <arglist>(float A, float B)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>resta</name>
      <anchorfile>libreria_8h.html</anchorfile>
      <anchor>a12052d25a22c6ed551d9b95edf4f9b0a</anchor>
      <arglist>(float A, float B)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>multiplicacion</name>
      <anchorfile>libreria_8h.html</anchorfile>
      <anchor>a7768dd53e17864f3487d42acc915ae18</anchor>
      <arglist>(float A, float B)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>main.cpp</name>
    <path>C:/Users/dj23/Documents/Gitlab/primeraclasecplusplus/Libreria/MiProyectoLibreria/</path>
    <filename>main_8cpp.html</filename>
    <member kind="function">
      <type>int</type>
      <name>main</name>
      <anchorfile>main_8cpp.html</anchorfile>
      <anchor>a0ddf1224851353fc92bfbff6f499fa97</anchor>
      <arglist>(int argc, char *argv[])</arglist>
    </member>
  </compound>
</tagfile>
