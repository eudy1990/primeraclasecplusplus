#include <iostream>
using namespace std;

/*
* crear apuntador
& pasar referencia de una variable 
*/

//Declaramos nuestro nodo
//Dato y apuntador
struct Nodo{
	int dato;//dato a guardar
	Nodo *siguiente;//apuntador del siguiente nodo
};

//Funcion para agregar nuevo item a la pila
void insertarDato(Nodo *&pila,int dato){
	//Creamos un nuevo nodo y lo almacenamos en un puntero(variable)
	Nodo *nuevo_nodo=new Nodo();
	
	nuevo_nodo->dato = dato;//asignamos el dato ingresado al nodo
	nuevo_nodo->siguiente =  pila;//asignamos el apuntador para que conecte con el nodo anterior
	
	pila = nuevo_nodo;//agregamos el nodo a la pila	
}
//sacar elemento de la pila 
void sacarItemPila(Nodo *&pila,int &dato){
	Nodo *aux = pila;//Utilizamos una variable temporal para almacenar el ultimo nodo agregado
	dato = aux->dato;//obtener el dato del nodo y guardar lo en la variable dato
	pila = aux->siguiente;//obtenemos el nodo que est� debajo del actual
	delete aux;//liberamos el espacio en memoria
	
}
int main(int argc, char *argv[]) {
	/*
	Una pila es una estructura de datos de entradas ordenadas tales que solo se pueden 
	introducir y eliminar por
	un extremo, llamado cima
	https://www.ilvo.es/12844-large/porta-platos.jpg
	https://users.dcc.uchile.cl/~bebustos/apuntes/cc30a/TDA/pila.gif
	lifo = last input first output
	
	push = insertar un elemento 
	pop = elimina un elemento o saca
	*/
	
	Nodo *pila=NULL;
	//Nodo *pila2;
	int dato;
	dato = 20;
	insertarDato(pila,dato);
	
	dato = 50;
	insertarDato(pila,dato);
	
	dato = 200;
	insertarDato(pila,dato);
	
	dato = 500;
	insertarDato(pila,dato);
	
	dato = 900;
	insertarDato(pila,dato);
	//int dato1;
	//pila2 = pila;
	cout<<" [ "<<" ";
	//bucle que verifica si hay elemento en la pila =
	while(pila != NULL){
		//funcion que se encarga de obtener el ultimo nodo de la pila
		sacarItemPila(pila,dato);
		cout<<dato<<" ";
		/*
		if(pila != NULL){
			cout<<dato<<" , ";
		}else{
			cout<<dato<<" ";
		}*/
	}
	cout<<" ]";

	
	return 0;
}


