#include <iostream>
using namespace std;

//definimos nuestra estructura
//esta se comporta como un tipo de dato personalizado por nosotros
struct Persona{
	string nombre;
	string apellido;
	int edad;
	char sexo;
	bool discapacitado;
};


int main(int argc, char *argv[]) {
	
	//Persona listaPersonas[2];
	
	Persona persona1;
	persona1.nombre ="Danilo";
	persona1.discapacitado = false;
	//persona1.discapacitado = 1;
	persona1.apellido="Arias";
	persona1.edad = 20;
	persona1.sexo = 'M';
	
	Persona persona2;
	persona2.nombre ="Hector";
	persona2.discapacitado = false;
	//persona2.discapacitado = 1;
	persona2.apellido="Evangelista";
	persona2.edad = 33;
	persona2.sexo = 'M';
	
	Persona persona3 = {"Eudy","Arias",18,'M',false};
	
	Persona persona4;
	cout<<"Nombre"<<endl;
	cin>>persona4.nombre;
	
	cout<<"Apellido"<<endl;
	cin>>persona4.apellido;
	
	cout<<"Edad"<<endl;
	cin>>persona4.edad;
	
	cout<<"Sexo"<<endl;
	cin>>persona4.sexo;
	
	cout<<"Discapacitado"<<endl;
	cin>>persona4.discapacitado;
	
	cout<<"persona1 nombre: "<<persona1.nombre<<endl;
	cout<<"persona1 apellido: "<<persona1.apellido<<endl;
	cout<<"persona1 sexo: "<<persona1.sexo<<endl;
	cout<<"persona1 edad: "<<persona1.edad<<endl;
	cout<<"persona1 discapacitado: "<<persona1.discapacitado<<endl;
	cout<<"============================================================"<<endl;
	cout<<"persona2 nombre: "<<persona2.nombre<<endl;
	cout<<"persona2 apellido: "<<persona2.apellido<<endl;
	cout<<"persona2 sexo: "<<persona2.sexo<<endl;
	cout<<"persona2 edad: "<<persona2.edad<<endl;
	cout<<"persona2 discapacitado: "<<persona2.discapacitado<<endl;
	cout<<"============================================================"<<endl;
	cout<<"persona3 nombre: "<<persona3.nombre<<endl;
	cout<<"persona3 apellido: "<<persona3.apellido<<endl;
	cout<<"persona3 sexo: "<<persona3.sexo<<endl;
	cout<<"persona3 edad: "<<persona3.edad<<endl;
	cout<<"persona3 discapacitado: "<<persona3.discapacitado<<endl;
	cout<<"============================================================"<<endl;
	cout<<"persona4 nombre: "<<persona4.nombre<<endl;
	cout<<"persona4 apellido: "<<persona4.apellido<<endl;
	cout<<"persona4 sexo: "<<persona4.sexo<<endl;
	cout<<"persona4 edad: "<<persona4.edad<<endl;
	cout<<"persona4 discapacitado: "<<persona4.discapacitado<<endl;
	
	
	
	
	return 0;
}

