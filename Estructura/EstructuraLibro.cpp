#include <iostream>
#include <string>
using namespace std;

struct Libro{
	string titulo;
	string autor;
	float precio;
	///Libro librosRelacionados;
};

int main(int argc, char *argv[]) {
	
	Libro libro1;
	Libro libro2 = {"Once minutos","Paulo Coelho",40};
	
	cout<<"Titulo libro 2 " << libro2.titulo<<endl;
	cout<<"Autor libro 2 " << libro2.autor<<endl;
	cout<<"Precio libro 2 " << libro2.precio<<endl;
	cout<<"==========================================="<<endl;
	libro1.autor = "Eudy Arias";
	cout << libro1.autor;
	
	libro1.titulo = "El alquimista";
	cout<<"El titulo del libro es: "<<libro1.titulo;
	cout<<endl;
	libro1.precio = 20.60;
	cout<<"El precio del libro "<<libro1.titulo<<" es: "<<libro1.precio;
	
	/*
	Una estructra que se llamara persona 
		nombre
		apellido
		edad
	Una estructra que se llamara empleado 
		nombre
		apellido
		edad
		sueldo
	*/
	
	
	
	return 0;
}

