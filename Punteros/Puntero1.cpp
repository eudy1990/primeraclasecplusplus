#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	
	int *puntero;
	int variableNumerica = 30;
	int v1=10,v2=20,resultado=0;
	
	//cout<<*puntero<<endl;
	cout<<variableNumerica<<endl;
	cout<<&variableNumerica<<endl;
	
	cout<<endl;
	
	puntero = &variableNumerica;
	
	cout<<"Dato del puntero "<<*puntero<<endl;
	
	*puntero = 6;
	cout<<"Dato del puntero "<<*puntero<<endl;
	cout<<"Dato de la variable "<<variableNumerica<<endl;
	
	cout<<"Variable manejadas con punteros "<<endl;
	int *puntero_v1;
	int *puntero_v2;
	int *puntero_resultado;
	
	puntero_v1 = &v1;
	puntero_v2 = &v2;
	puntero_resultado = &resultado;
	
	*puntero_resultado = (*puntero_v1+*puntero_v2);
	
	cout<<"La suma de los punteros es "<<*puntero_resultado<<endl;
	
	
	/*
	Realizar la suma, resta, multiplicacion, division, con dos valores en codigo v1 y v2 
	
	*/
	
	
	
	return 0;
}

