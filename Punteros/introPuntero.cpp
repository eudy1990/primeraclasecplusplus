#include <iostream>
using namespace std;

struct Libro{
	string titulo;
	string autor;
	float precio;
};

int main(int argc, char *argv[]) {
	
	/*
	&puntero direccion de memoria
	*puntero la variable cuya direccion esta almacenada en puntero
	
	*/
	
	int numero;
	numero = 20;
	
	cout<<"Dato almacenado en la variable numero: "<<numero<<endl;
	cout<<"Direccion en memoria de la variable numero: "<<&numero<<endl;
	
	
	//Declarando un puntero
	//El puntero debe ser del mismo tipo de datos que va almacenar
	//Tipos de datos char, int, float, string
	int *puntero_variable_numero;//Declaracion del puntero
	puntero_variable_numero = &numero;//almacenar direccion de memoria de la variable numero en el puntero creado puntero_variable_numero
	
	cout<<"Dato almacenado en la variable numero (Puntero): "<<*puntero_variable_numero<<endl;
	cout<<"Direccion en memoria de la variable numero (Puntero): "<<puntero_variable_numero<<endl;
	
	//Sumar le 30 al valor de la variable numero desde el puntero
	*puntero_variable_numero = *puntero_variable_numero + 30;
	cout<<"Dato almacenado en la variable numero (Puntero): "<<*puntero_variable_numero<<endl;
	cout<<"Dato almacenado en la variable numero: "<<numero<<endl;
	/*
	Con este ejemplo podemos observar que el puntero y la variable est�n conectado ya que est�n 
	obteniendo su dato desde el mismo espacio de la memoria por este motivo si alteramos el valor 
	se ve reflejado tanto en el puntero como en la variable.
	*/
	//Ejemplo de puntero para manejar un arreglo
	cout<<endl;
	int numeros[5] = {1,2,3,4,5};
	int *puntero_array = numeros;
	for(int i=4; i>=0; i--){
		cout<<puntero_array[i]<<" ";
	}
	
	//Ejemplo de puntero para manejar estructura
	Libro libro1 = {"El Alquimista","paulo coelho",20.50};
	Libro libro2 = {"La quinta monta�a","paulo coelho",50.50};
	Libro *puntero_libro = &libro1;//&libro1 pasamos la direccion de memoria de la estructura creada 
	Libro puntero_libro2 = libro2;//No es un puntero  
	
	cout<<endl;
	//para acceder a los elementos de la estructura desde un puntero debemos usar el simbolo ->
	cout<<"Titulo: "<< puntero_libro->titulo <<endl;
	cout<<"Autor: "<< puntero_libro->autor <<endl;
	cout<<"Precio: "<< puntero_libro->precio <<endl;
	
	//para acceder a los elementos de la estructura desde un puntero debemos usar el simbolo ->
	cout<<"Titulo: "<< puntero_libro2.titulo <<endl;
	cout<<"Autor: "<< puntero_libro2.autor <<endl;
	cout<<"Precio: "<< puntero_libro2.precio <<endl;
	
	
	return 0;
}

