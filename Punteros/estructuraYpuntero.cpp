#include <iostream>
using namespace std;

struct Persona{
	string nombre;
	string apellido;
};

int main(int argc, char *argv[]) {
	Persona persona1;
	
	Persona *puntero_persona1;
	
	puntero_persona1 = &persona1;
	
	puntero_persona1->nombre= "Eudy";
	
	puntero_persona1->apellido= "Arias";
	
	cout << puntero_persona1->nombre<<endl;
	cout << puntero_persona1->apellido<<endl;
	
	
	/*
	estructura llamada empleado con los siguientes campos (puntero)
		nombreCompleto
		edad
	*/
	
	
	
	return 0;
}

