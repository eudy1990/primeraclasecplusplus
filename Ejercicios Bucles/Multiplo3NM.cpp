#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int inicio=0,fin=0;
	int sumaMultiplos3=0;
	cout<<"Ingrese un numero de inicio"<<endl;
	cin>>inicio;
	cout<<"Ingrese un numero de fin"<<endl;
	cin>>fin;
	
	for(int i = inicio; i<= fin; i=i+1){
		cout<<i<<endl;
		if((i%3) == 0){
			cout<<"Es multiplo de 3"<<endl;
			sumaMultiplos3 = sumaMultiplos3 + i;
		}
	}
	cout<<"La suma total "<<sumaMultiplos3<<endl;
	
	return 0;
}

