#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int n=0,d=0,sumaDivisores=0;
	
	cout<<"Ingrese un numero"<<endl;
	cin>>n;
	cout<<"Ingrese un divisor"<<endl;
	cin>>d;
	
	if((n%d)==0){
		cout<<"El numero "<<d<<" Es un divisor de "<<n<<endl;
	}else{
		cout<<"El numero "<<d<<" No Es un divisor de "<<n<<endl;
	}
	for(int i=1;i<n;i=i+1){
		//cout<<i<<endl;
		if((n%i)==0){
			sumaDivisores = sumaDivisores + i;
			cout<<"El numero "<<i<<" Es un divisor de "<<n<<endl;
		}
	}
	
	cout<<"La suma de los divisores es "<<sumaDivisores<<endl;
	if(sumaDivisores == n){
		cout<<"El numero "<<n<<" Es un numero perfecto"<<endl;
	}else{
		cout<<"El numero "<<n<<" No Es un numero perfecto"<<endl;
	}
	
	return 0;
}

