#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	
	int numero = 0;
	//bucle del 1 al 10
	//variable que almacene la suma de todos los numeros pares
	int sumaPares = 0;
	//variable que almacene la suma de todos los numeros impares pares
	int sumaImpares = 0;
	//contador de todos los numeros pares encontrados
	int contadorPare=0;
	//variable que almacenara el promedio de los numeros pares
	int promedioPar=0;
	
	
	for(int i = 1; i<=10;i=i+1){
		cout<<"Peticion "<<i<<endl;
		cout<<"Ingrese por favor un numero"<<endl;
		cin>>numero;
		if( (numero%2)==0 ){ //Es un numero par
			sumaPares = sumaPares + numero;
			contadorPare = contadorPare + 1;
			
		}else{//Es un numero impar
			sumaImpares = sumaImpares + numero;
		}
	}
	
	promedioPar = sumaPares/contadorPare;
	
	cout<<"La suma de los impares es : "<< sumaImpares<<endl;
	cout<<"El promedio de los numeros par : "<< promedioPar<<endl;
	
	
	return 0;
}

