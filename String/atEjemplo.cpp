#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	string saludo = "hola programadores";
	cout<<saludo[0]<<" - "<<saludo[3]<<endl;
	cout<<saludo.at(0)<<" - "<<saludo.at(3)<<endl;
	for(int i = 0 ; i<saludo.size() ; i++){
		char caracter = saludo.at(i);
		cout<<caracter<<endl;
	}
	return 0;
}

