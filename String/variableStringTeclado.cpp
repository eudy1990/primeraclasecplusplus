#include <iostream>
#include <string.h>
using namespace std;

int main(int argc, char *argv[]) {
	string texto;
	int contadorMayuscula = 0;
	int contadorMinuscula = 0;	
	int contadorAZaz = 0;
	int contadorDigitos = 0;
	int contadorPuntos = 0;
	
	cout<<"Favor ingresar un texto"<<endl;
	getline(cin,texto,'\n');
	
	for(int i= 0 ; i<(texto.size());i++ ){
		char caracter = texto[i];
		//65 = A y 90 = Z
		if(caracter >= 65 && caracter <= 90) 
			contadorMayuscula++;
		//97 = a y 122=z
		if(caracter >= 97 && caracter <= 122) 
			contadorMinuscula++;
		//a-z A-Z
		if((caracter >= 65 && caracter <= 90)||(caracter >= 97 && caracter <= 122))
			contadorAZaz++;
		//48 = 0 y  57=9
		if(caracter >= 48 && caracter <= 57)
			contadorDigitos++;
		//46 = .
		if(caracter == 46)
			contadorPuntos++;
		
		
	}
	
	cout.width(23);
	cout<<"Total Mayusculas ";
	cout.width(5);
	cout<<" : ";
	cout.width(10);
	cout<<contadorMayuscula;
	cout<<endl;
	cout.width(23);
	cout<<"Total Minuscula ";
	cout.width(5);
	cout<<" : ";
	cout.width(10);
	cout<<contadorMinuscula;
	cout<<endl;
	cout.width(23);
	cout<<"Total Letra alfabeto ";
	cout.width(5);
	cout<<" : ";
	cout.width(10);
	cout<<contadorAZaz;
	cout<<endl;
	cout.width(23);
	cout<<"Total Digito ";
	cout.width(5);
	cout<<" : ";
	cout.width(10);
	cout<<contadorDigitos;
	cout<<endl;
	cout.width(23);
	cout<<"Total Punto ";
	cout.width(5);
	cout<<" : ";
	cout.width(10);
	cout<<contadorPuntos;
	cout<<endl;
	
	return 0;
}

