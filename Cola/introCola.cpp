#include <iostream>
using namespace std;


struct Nodo{
	int dato;
	Nodo *siguiente;
};

void insertarItemCola(Nodo *&frente, Nodo *&fin, int dato){
	Nodo *nuevo_nodo = new Nodo(); //creamos nuevo espacio en memoria
	nuevo_nodo->dato = dato; // asignamos el dato que queremos guardar
	nuevo_nodo->siguiente = NULL;//Lo ponemos en null
	
	//validar si es el unico elemento o no, si es null es porque no hay nodo agregado
	if(frente == NULL){
		frente = nuevo_nodo;
	}else{
		fin->siguiente = nuevo_nodo;
	}
	fin = nuevo_nodo;
}
void sacarItemCola(Nodo *&frente, Nodo *&fin, int &dato){
	dato = frente->dato;//extraemos el dato del elemento que esta al frente de la cola
	Nodo *auxFrente = frente;//manejamos un auxiliar
	
	//si frente y fin son iguales es por que hay un elemento en la cola
	if(frente == fin){
		//como no hay mas elementos podemos borrar tanto a frente como a fin;
		frente = NULL;
		fin = NULL;
		
	}else{//significa que hay mas elementos en la cola
		//como hay mas elementos entonces debemos cambiar al frente por el elemnto que estaba antes
		frente = frente->siguiente;
		
	}
	
	delete auxFrente; // eliminamos a auxiliar frente;
	
}
		
	

int main(int argc, char *argv[]) {
	/*
		Una cola es una estructura de datos, caracterizada por ser una secuencia 
		de elementos en la que la operacion de insercion se realiza por un extremo
		la extraccion por el otro.
	https://www.sistemaimpulsa.com/blog/wp-content/uploads/2019/03/Fila-de-personas-619x346.jpg
	https://image.shutterstock.com/image-vector/fifo-icon-goods-handling-procedure-260nw-1849454173.jpg
	https://www.repsly.com/hs-fs/hubfs/stock-rotation-FIFO-gif.gif?width=500&name=stock-rotation-FIFO-gif.gif
	
	FIFO = primero en entrar primero en salir 
	
	apara implementar las colas es lo mismo que la pila pero con un poco mas de logica
	
	push = agrega un nuevo elemento a la cola
	pop = saca un elemento de la cola
	
	*/
	
	//insertar elementos a la cola
	Nodo *frente = NULL;
	Nodo *fin = NULL;
	int dato = 0;
	
	dato = 90;
	insertarItemCola(frente,fin,dato);
	
	dato = 40;
	insertarItemCola(frente,fin,dato);
	
	dato = 100;
	insertarItemCola(frente,fin,dato);
	
	dato = 200;
	insertarItemCola(frente,fin,dato);
	
	//mostrar elementos de la cola
	cout<<" [ ";
	while(frente != NULL){
		sacarItemCola(frente,fin,dato);
		if(frente != NULL){
			cout<<dato<<" , ";
		}else{
			cout<<dato<<" . ";
		}
		
	}
	cout<<" ] ";
	
	return 0;
}

