#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	//Ordenamiento por seleccion
	int arregloAOrdenar[5] = {3,2,1,5,4};
	int auxNumero=0,posicionNumeroMinimo=0;
	
	//Mostrar los numeros del arreglo, sin ordenar
	cout<<" Arreglo sin ordenar : ";
	for(int i = 0 ; i<5;i++){
		cout<<arregloAOrdenar[i]<<" ";
	}
	cout<<endl;
	
	//Ordenamos el arreglo
	for(int revision = 0 ; revision<5;revision++){
		//almacenamos la primera posicion en donde se guardara 
		//el numero mas peque�o
		posicionNumeroMinimo = revision;
		//Bucle que se encarga de verificar el numero mas peque�o del arreglo
		//(revision+1) = Saltamos de verificar la primera posicion del arreglo 
		//porque en ella es que guardaremos el numero mas peque�o
		for(int posicion = (revision+1) ; posicion<5;posicion++){
			//Verificamos si existe un elemento menor al anterior que hab�amos marcado
			if(arregloAOrdenar[posicion] < arregloAOrdenar[posicionNumeroMinimo]){
				//Cuando encontramos un numero mas peque�o, guardamos esa posicion
				posicionNumeroMinimo = posicion;
			}
		}
		//Cuando termina nuestro bucle es porque encontro el dato o numero 
		//mas peque�o de los que estan en el arreglo
		auxNumero = arregloAOrdenar[revision];
		arregloAOrdenar[revision] = arregloAOrdenar[posicionNumeroMinimo];
		arregloAOrdenar[posicionNumeroMinimo] = auxNumero;
		
	}
	
	
	//Mostrar los numeros del arreglo, ordenado
	cout<<" Arreglo ordenado : ";
	for(int i = 0 ; i<5;i++){
		cout<<arregloAOrdenar[i]<<" ";
	}
	
	return 0;
}

