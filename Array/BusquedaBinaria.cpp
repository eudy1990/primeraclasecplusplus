#include <iostream>
using namespace std;

int getMitad(int inferior,int superior){
	int mitad = (inferior+superior)/2;
	cout<<mitad<<endl;
	return mitad;
}
int main(int argc, char *argv[]) {
	//Busqueda binaria 
	
	//Se necesita un arreglo ordenado 
	int numeros[5] = {1,2,3,4,5};//arreglo ordenado
	int inferior=0,superior=0,mitad=0,datoABuscar=0;
	bool existe=false;//Variable que identifica si encontro el dato o no
	
	inferior = 0;//Posicion del arreglo
	superior = 5;//Posicion del arreglo
	datoABuscar=4;
	
	//Bucle para repetir la acciones
	while(inferior <= superior){
		//obtenemos la mitad de la cantidad de elementos que tiene el arreglo
		//mitad = getMitad(inferior,superior);
		mitad = (inferior+superior)/2;
		
		//validamos si el dato que buscamos esta en el media del arreglo
		int numero = numeros[mitad];
		if (numero==datoABuscar) {
			existe = true;
			break;//Salir del bucle
		}else if (numeros[mitad]>datoABuscar) {
			superior = mitad-1;
		}else if (numeros[mitad]<datoABuscar) {
			inferior = mitad+1;
		}
	}
	if(existe == true){
		cout<<"Se encontro el dato "<<datoABuscar<<" en la posicion "<<mitad<<endl;
	}else{
		cout<<"No Se encontro el dato "<<datoABuscar<<endl;
	}
	
	
	return 0;
}

