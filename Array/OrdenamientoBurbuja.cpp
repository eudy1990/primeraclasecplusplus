#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	//Ordenamiento Burbuja
	int arregloAOrdenar[5] = {4,6,7,8,0};
	//for para revisar el arreglo
	for(int revision = 0; revision<5;revision++){
		//For Ordenar elementos
		for(int i=0 ; i<5 ; i++){
			//mover elemento si en la posicion actual hay un numero inferior a la siguiente posicion 
			if(arregloAOrdenar[i] > arregloAOrdenar[(i+1)]){
				int aux = arregloAOrdenar[(i+1)];
				arregloAOrdenar[(i+1)] = arregloAOrdenar[i];
				arregloAOrdenar[i] = aux;
			}
		}
	}
	//mostra elementos
	for(int i = 0; i<5;i++){
		cout<<arregloAOrdenar[i]<<" ";
	}
	
	
	return 0;
}

