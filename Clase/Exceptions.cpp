#include <iostream>
using namespace std;
/*
El manejo de excepciones en C ++ consiste en tres palabras clave: try, throwy catch:

La trydeclaraci�n le permite definir un bloque de c�digo que se probar� en busca de errores mientras se ejecuta.

La throwpalabra clave lanza una excepci�n cuando se detecta un problema, lo que nos permite crear un error personalizado.

La catchdeclaraci�n le permite definir un bloque de c�digo que se ejecutar�, si ocurre un error en el bloque try.

Las palabras clave tryy catchvienen en pares:
*/
int main(int argc, char *argv[]) {
	
//	try {
//		// Block of code to try
//		throw exception; // Throw an exception when a problem arise
//	}
//	catch () {
//		// Block of code to handle errors
//	}
	
	try {
		int age = 15;
		if (age >= 18) {
			cout << "Access granted - you are old enough.";
		} else {
			throw (age);
		}
	}
	catch (int myNum) {
		cout << "Access denied - You must be at least 18 years old.\n";
		cout << "Age is: " << myNum;
	}
	
//	
//	try {
//		int age = 15;
//		if (age >= 18) {
//			cout << "Access granted - you are old enough.";
//		} else {
//			throw 505;
//		}
//	}
//	catch (int myNum) {
//		cout << "Access denied - You must be at least 18 years old.\n";
//		cout << "Error number: " << myNum;
//	}
//	
//	try {
//		int age = 15;
//		if (age >= 18) {
//			cout << "Access granted - you are old enough.";
//		} else {
//			throw 505;
//		}
//	}
//	catch (...) {
//		cout << "Access denied - You must be at least 18 years old.\n";
//	}
//	
	return 0;
}

