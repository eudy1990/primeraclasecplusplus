#include <iostream>
using namespace std;
/*
Herencia
En C ++, es posible heredar atributos y m�todos de una clase a otra. Agrupamos el "concepto de herencia" en dos categor�as:

clase derivada (hijo): la clase que hereda de otra clase
clase base (padre): la clase que se hereda de
Para heredar de una clase, use el :s�mbolo.
*/
// Base class
class Vehicle {
public:
	string brand = "Ford";
	void honk() {
		cout << "Tuut, tuut! \n" ;
	}
};

// Derived class
class Car: public Vehicle {
public:
	string model = "Mustang";
};


int main(int argc, char *argv[]) {
	
	Car myCar;
	myCar.honk();
	cout << myCar.brand + " " + myCar.model;
	
	return 0;
}

