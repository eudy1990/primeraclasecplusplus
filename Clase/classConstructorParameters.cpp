#include <iostream>
using namespace std;

class Car {        // The class
public:          // Access specifier
	string brand;  // Attribute
	string model;  // Attribute
	int year;      // Attribute
	Car(string x, string y, int z) { // Constructor with parameters
		brand = x;
		model = y;
		year = z;
	}
};

class Car1 {        // The class
public:          // Access specifier
	string brand;  // Attribute
	string model;  // Attribute
	int year;      // Attribute
	Car1(string x, string y, int z); // Constructor declaration
};

// Constructor definition outside the class
Car1::Car1(string x, string y, int z) {
	brand = x;
	model = y;
	year = z;
}


int main(int argc, char *argv[]) {
	
	// Create Car objects and call the constructor with different values
	Car carObj1("BMW", "X5", 1999);
	Car carObj2("Ford", "Mustang", 1969);
	
	// Print values
	cout << carObj1.brand << " " << carObj1.model << " " << carObj1.year << "\n";
	cout << carObj2.brand << " " << carObj2.model << " " << carObj2.year << "\n";
	
	
	
//	// Create Car objects and call the constructor with different values
//	Car1 car1Obj1("BMW", "X5", 1999);
//	Car1 car1Obj2("Ford", "Mustang", 1969);
//	
//	// Print values
//	cout << car1Obj1.brand << " " << car1Obj1.model << " " << car1Obj1.year << "\n";
//	cout << car1Obj2.brand << " " << car1Obj2.model << " " << car1Obj2.year << "\n";
	return 0;
}

