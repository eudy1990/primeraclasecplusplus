#include <iostream>
using namespace std;
class MyClass {        // The class
public:              // Access specifier
	void myMethod() {  // Method/function defined inside the class
		cout << "Hello World!";
	}
};

class MyClass2 {        // The class
public:              // Access specifier
	void myMethod();   // Method/function declaration
};

// Method/function definition outside the class
void MyClass2::myMethod() {
	cout << "Hello World!";
}

int main(int argc, char *argv[]) {
	
	
	MyClass myObj;     // Create an object of MyClass
	myObj.myMethod();  // Call the method
	
	MyClass2 myObj2;     // Create an object of MyClass
	myObj2.myMethod();  // Call the method
	
	
	return 0;
}

