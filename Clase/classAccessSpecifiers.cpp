#include <iostream>
using namespace std;
/*
public - los miembros son accesibles desde fuera de la clase
private - no se puede acceder (o ver) a los miembros desde fuera de la clase
protected- No se puede acceder a los miembros desde fuera de la clase, sin embargo, se puede acceder a ellos en clases heredadas. M�s adelante aprender� m�s sobre la herencia .
*/
class MyClass {
public:    // Public access specifier
	int x;   // Public attribute
private:   // Private access specifier
	int y;   // Private attribute
};


int main(int argc, char *argv[]) {
	
	MyClass myObj;
	myObj.x = 25;  // Allowed (public)
	myObj.y = 50;  // Not allowed (private)
	
	return 0;
}

