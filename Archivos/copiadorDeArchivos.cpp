#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char *argv[]) {
	ifstream archivoOriginal("datos.txt", ios::in);
	ofstream archivoCopia("datosCopia.txt");  // constructora de ofstream 
	
	
	if(archivoOriginal.fail())
		cerr << "Archivo no encontrado" << endl;//Error
	else
		while(!archivoOriginal.eof())
	{
		//leer la linea
		string texto = "";
		getline(archivoOriginal,texto);
		cout<<texto<<endl;
		//copiar linea leida
		archivoCopia <<texto<< endl;
		
	}
	
		archivoOriginal.close();//Cierra el archivo
		archivoCopia.close();//Cierra el archivo
		
	
	return 0;
}

