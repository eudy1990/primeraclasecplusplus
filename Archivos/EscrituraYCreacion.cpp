#include <fstream>//libreria para manejar archivo; Nota debe estar de primero
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
//	ofstream archivo;  // objeto de la clase ofstream
//	archivo.open("Carpeta\\datos.txt");
//	
	ofstream archivo("datos1.txt");  // constructora de ofstream 
	
	//Nota>No se recomienda agregar acentos
	archivo << "Primera l�nea de texto" << endl;
	archivo << "Segunda l�nea de texto" << endl;
	archivo << "Ultima l�nea de texto" << endl;
	
	string texto = "";
	cout<<"Ingresar un texto "<<endl;
	getline(cin,texto,'\n');
	archivo << texto.c_str() << endl;
//	
	archivo.close();//Cierra el archivo
	
	/*
	Al especificar la apertura de un archivo como se ha mostrado en los programas anteriores, 
	el programa sobreescribe cualquier archivo existente llamado Datos.txt en el directorio de 
	trabajo del programa. Dependiendo del prop�sito del programa es posible que sea necesario 
	agregar datos a los ya existentes en el archivo, o quiz� sea necesario que las operaciones 
	del programa no se lleven a cabo en caso de que el archivo especificado exista en el disco, 
	para �stos casos podemos especificar el modo de apertura del archivo incluyendo un par�metro 
	adicional en el constructor, cualquiera de los siguientes:
	*/
//	ios::app Operaciones de a�adidura.
//		
//		ios::ate Coloca el apuntador del archivo al final del mismo.
//		
//		ios::in Operaciones de lectura. Esta es la opci�n por defecto para objetos de la clase ifstream.
//		
//		ios::out Operaciones de escritura. Esta es la opci�n por defecto para objetos de la clase ofstream.
//		
//		ios::nocreate Si el archivo no existe se suspende la operaci�n.
//		
//		ios::noreplace Crea un archivo, si existe uno con el mismo nombre la operaci�n se suspende.
//		
//		ios::trunc Crea un archivo, si existe uno con el mismo nombre lo borra.
//		
//		ios::binary Operaciones binarias.
	//ofstream archivo("Datos.txt", ios::app);.//concatenen en el archivo Datos.txt
	//ofstream archivo("Datos.txt", ios::noreplace);//no sobreescriba un archivo existente
	return 0;
}

