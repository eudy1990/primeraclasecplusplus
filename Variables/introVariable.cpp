#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	//Variable que asume el tipo de dato
//	auto variable = "";
//	variable = "texto";
//	cout<<variable<<endl;
	
	//Convertir de una caracter a su representacion en decimal
	//char a = 'b';
	//int aI = (int)a;
	//cout<<aI<<endl;
	
	//Declaracion de una variable
	int numero;
	float sueldo;
	char vocal;
	bool discapacitado;
	string institucion;
	
	//Asignacion de dato a una variable
	 numero = 20;
     sueldo=100000;
	 vocal='a';
	 discapacitado=false;
	 institucion="Infotep";
	
	//Declaracion y inicializacion de una variable
	 int edad = 18;
	 float descuento = 34.99;
	 char genero = 'M';
	 string producto = "silla";
	 bool continuar = true;
	 
	
	//Declaracion y inicializacion de una constante
	const float PI = 3.1416;//Declaracion de una constante
	cout<<PI<<endl;
	
	return 0;
}

