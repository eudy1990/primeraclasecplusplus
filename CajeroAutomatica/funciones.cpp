#include <iostream>
#include <cstdlib>
#include <windows.h>
using namespace std;
void consoleCLS(){
	system("cls");
}
void stop(){
	system("PAUSE");
}
void cargarCuentas(){
	listaCuentas[0].index = 0;
	listaCuentas[0].cuenta = 100000000001;
	listaCuentas[0].pin = "1234" ;
	listaCuentas[0].balanceActual = 1000000;
	
	listaCuentas[1].index = 1;
	listaCuentas[1].cuenta = 100000000002;
	listaCuentas[1].pin = "5678" ;
	listaCuentas[1].balanceActual = 90050000;
	
	listaCuentas[2].index = 2;
	listaCuentas[2].cuenta = 100000000003;
	listaCuentas[2].pin = "9012" ;
	listaCuentas[2].balanceActual = 700000;
	
	listaCuentas[3].index = 3;
	listaCuentas[3].cuenta = 100000000004;
	listaCuentas[3].pin = "0987" ;
	listaCuentas[3].balanceActual = 12000;
}
bool login(long long int cuenta, string pin){
	bool resultado = false;
	for(int i = 0 ; i < sizeof(listaCuentas)/sizeof(CuentaAhorro) ; i++){
		if(cuenta == listaCuentas[i].cuenta && pin == listaCuentas[i].pin){
			cuentaActual = listaCuentas[i];
			resultado = true;
			break;
		}
	}
	return resultado;
}
	string consoleReadWithPrintLn(string letrero){
		string numero;
		cout<<letrero<<endl;
		getline(cin,numero,'\n');
		return numero;
	}
		
bool esSoloDigito(string numero){
			
			bool resultado = true;
			for(int i = 0 ; i<numero.size(); i++){
				if(!isdigit(numero[i])){
					resultado = false;
					break;
				}
			}
			return resultado;
}
			
			int convertStringToInt(string numero){
				return atoi(numero.c_str());
			}
				void errorConvertToLongLongInt(){
					cout<<"Favor ingresar solo numero "<<endl;
				}			
	int consoleGetOpcion(){
		string opcionS = consoleReadWithPrintLn("favor ingresar la opci�n deseada");
		if(esSoloDigito(opcionS)){
			return convertStringToInt(opcionS);
		}else{
			errorConvertToLongLongInt();
		}
		return -1;
	}
		void optionConfirm(int op,long long int montoRetirado){
			if(op == 1){
				letreroPrincipalConsultaBalanceImprimirRecibo(cuentaActual.balanceActual, montoRetirado);
			}
			//stop();
		}
void selectOptionConfirm(long long int montoRetirado){
	letreroDeseaImprimirRecibo();
	int opcion = consoleGetOpcion();
	optionConfirm(opcion,montoRetirado);
}
bool realizarRetiro(long long int monto){
	bool resultado = false;
	if(((listaCuentas[cuentaActual.index].balanceActual - monto)>0)){
		listaCuentas[cuentaActual.index].balanceActual -= monto;
		cuentaActual = listaCuentas[cuentaActual.index];
		resultado = true;
		selectOptionConfirm(monto);
	}
	
	return resultado;
}
long long int convertStringToLongLongInt(string numero){
	return atoll(numero.c_str());
}
void errorUsuarioOClaveIncorrecta(){
	cout<<"usuario o clave incorrecto"<<endl;
}
void warningMontoInsuficiente(){
	cout<<"El monto ingresado excede al balance disponible"<<endl;
}

long long int consoleGetCuenta(){
	string cuentaS = consoleReadWithPrintLn("favor ingresar el numero de cuenta");
	if(esSoloDigito(cuentaS)){
		return convertStringToLongLongInt(cuentaS);
	}else{
		errorConvertToLongLongInt();
	}
	return -1;
}
long long int consoleGetMontoRetiro(){
		string montoS = consoleReadWithPrintLn("favor ingresar el monto a retirar");
		if(esSoloDigito(montoS)){
			return convertStringToLongLongInt(montoS);
		}else{
			errorConvertToLongLongInt();
		}
		return -1;
}
string consoleGetPin(){
	string pinS = consoleReadWithPrintLn("favor ingresar el pin");
	return pinS;
}

long long int getBalanceActual(){
	return cuentaActual.balanceActual;
}

void opcionConsultaBalance(int op){
		if(op == 1){
			letreroPrincipalConsultaBalanceMostrarPorPantalla(getBalanceActual());
		}else if(op == 2){
			letreroPrincipalConsultaBalanceImprimirRecibo(getBalanceActual());
		}
		stop();
}
void retiroEfectivoPersonalizado(){
	long long int montoRetiro = consoleGetMontoRetiro(); 
	if(montoRetiro > 0){
		if(!realizarRetiro(montoRetiro))
			warningMontoInsuficiente();
	}
	
}
void opcionRetiroEfectivo(int op){
	if(op == 1){
		realizarRetiro(100);
	}else if(op == 2){
		realizarRetiro(200);
	}else if(op == 3){
		realizarRetiro(500);
	}else if(op == 4){
		realizarRetiro(1000);
	}else if(op == 5){
		realizarRetiro(2000);
	}else if(op == 6){
		retiroEfectivoPersonalizado();
	}
	stop();
}

void selectOptionConsultaBalance(){
	letreroPrincipalConsultaBalance();
	int opcion = consoleGetOpcion();
	opcionConsultaBalance(opcion);
}
void selectOptionRetiroEfectivo(){
	letreroPrincipalRetiroEfectivo();
	int opcion = consoleGetOpcion();
	opcionRetiroEfectivo(opcion);
}
void opcionMain(int op){
	if(op == 1){
		selectOptionConsultaBalance();
	}else if(op == 2){
		selectOptionRetiroEfectivo();
	}
}
void selectOpcionMain(){
	int opcion=0;
	do{
	consoleCLS();
	letreroPrincipal();
	opcion = consoleGetOpcion();
	opcionMain(opcion);
	}while(opcion != 3);
}	
void entrarSistema(){
	letreroLogin();
	long long int noCuenta = consoleGetCuenta();
	string pin = consoleGetPin();
	if(login(noCuenta, pin)){
		selectOpcionMain();
	}else{
		errorUsuarioOClaveIncorrecta();
	}
}
void ini(){
	cargarCuentas();
	entrarSistema();
	
}
