#include <iostream>
using namespace std;
void letreroPrincipal(){
	cout<<"================================================"<<endl;
	cout<<"=============                      ============="<<endl;
	cout<<"=============                      ============="<<endl;
	cout<<"=============   CAJERO AUTOMATICO  ============="<<endl;
	cout<<"=============                      ============="<<endl;
	cout<<"=============                      ============="<<endl;
	cout<<"=============      OPCIONES        ============="<<endl;
	cout<<"============= 1. CONSULTAR BALANCE ============="<<endl;
	cout<<"============= 2. RETIRO EFECTIVO   ============="<<endl;
	cout<<"============= 3. SALIR             ============="<<endl;
	cout<<"=============                      ============="<<endl;
	cout<<"=============                      ============="<<endl;
	cout<<"================================================"<<endl;
	cout<<""<<endl;
	cout<<""<<endl;
}
void letreroPrincipalConsultaBalance(){
		cout<<"================================================="<<endl;
		cout<<"===========                         ============="<<endl;
		cout<<"===========                         ============="<<endl;
		cout<<"===========     CAJERO AUTOMATICO   ============="<<endl;
		cout<<"===========                         ============="<<endl;
		cout<<"===========                         ============="<<endl;
		cout<<"===========        OPCIONES         ============="<<endl;
		cout<<"=========== 1. MOSTRAR POR PANTALLA ============="<<endl;
		cout<<"=========== 2. IMPRIMIR RECIBO      ============="<<endl;
		cout<<"===========                         ============="<<endl;
		cout<<"===========                         ============="<<endl;
		cout<<"================================================="<<endl;
		cout<<""<<endl;
		cout<<""<<endl;
}

void letreroPrincipalConsultaBalanceImprimirRecibo(long long int balanceActual){
			cout<<"================================================="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========     CAJERO AUTOMATICO   ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========         IMPRIMIR        ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========          RECIBO         ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========      BALANCE ACTUAL     ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========   ";
			cout.width(15);
			cout<<balanceActual;
			cout<<"       ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"===========                         ============="<<endl;
			cout<<"================================================="<<endl;
			cout<<""<<endl;
			cout<<""<<endl;
}		

			void letreroPrincipalConsultaBalanceImprimirRecibo(long long int balanceActual, long long int montoRetirado){
				cout<<"================================================="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========     CAJERO AUTOMATICO   ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========         IMPRIMIR        ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========          RECIBO         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========      BALANCE ACTUAL     ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========   ";
				cout.width(15);
				cout<<balanceActual;
				cout<<"       ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"=========== MONTO RETIRADO          ============="<<endl;
				cout<<"===========   ";
				cout.width(15);
				cout<< montoRetirado;
				cout<<"       ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"================================================="<<endl;
				cout<<""<<endl;
				cout<<""<<endl;
			}				
void letreroPrincipalConsultaBalanceMostrarPorPantalla(long long int balanceActual){
				cout<<"================================================="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========     CAJERO AUTOMATICO   ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========      BALANCE ACTUAL     ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========   ";
				cout.width(15);
				cout<<balanceActual;
				cout<<"       ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"===========                         ============="<<endl;
				cout<<"================================================="<<endl;
				cout<<""<<endl;
				cout<<""<<endl;
}		
				
			

		void letreroPrincipalRetiroEfectivo(){
			cout<<"========================================================"<<endl;
			cout<<"==============                         ================="<<endl;
			cout<<"==============                         ================="<<endl;
			cout<<"==============     CAJERO AUTOMATICO   ================="<<endl;
			cout<<"==============                         ================="<<endl;
			cout<<"==============                         ================="<<endl;
			cout<<"===========           OPCIONES             ============="<<endl;
			cout<<"===========                                ============="<<endl;
			cout<<"=========== 1. $100                 200 .2 ============="<<endl;
			cout<<"=========== 3. $500                1000 .4 ============="<<endl;
			cout<<"=========== 5. $2000      Otra Cantidad .6 ============="<<endl;
			cout<<"===========                                ============="<<endl;
			cout<<"==============                         ================="<<endl;
			cout<<"==============                         ================="<<endl;
			cout<<"========================================================"<<endl;
			cout<<""<<endl;
			cout<<""<<endl;
		}
			void letreroPrincipalRetiroEfectivoOtraCantidad(){
				cout<<"========================================================"<<endl;
				cout<<"==============                         ================="<<endl;
				cout<<"==============                         ================="<<endl;
				cout<<"==============     CAJERO AUTOMATICO   ================="<<endl;
				cout<<"==============                         ================="<<endl;
				cout<<"==============                         ================="<<endl;
				cout<<"===========           OPCIONES             ============="<<endl;
				cout<<"===========                                ============="<<endl;
				cout<<"===========      INGRESE LA CANTIDAD       ============="<<endl;
				cout<<""<<endl;
				cout<<""<<endl;
			}
void letreroDeseaImprimirRecibo(){
	cout<<"========================================================"<<endl;
	cout<<"==============                         ================="<<endl;
	cout<<"==============                         ================="<<endl;
	cout<<"==============     CAJERO AUTOMATICO   ================="<<endl;
	cout<<"==============                         ================="<<endl;
	cout<<"==============                         ================="<<endl;
	cout<<"===========           OPCIONES             ============="<<endl;
	cout<<"===========                                ============="<<endl;
	cout<<"===========  �Desea imprimir el recibo?    ============="<<endl;
	cout<<"===========                                ============="<<endl;
	cout<<"===========  1. SI                NO .2    ============="<<endl;
	cout<<"===========                                ============="<<endl;
	cout<<""<<endl;
	cout<<""<<endl;
}
void letreroLogin(){
				cout<<"================================================"<<endl;
				cout<<"=============                      ============="<<endl;
				cout<<"=============                      ============="<<endl;
				cout<<"=============   CAJERO AUTOMATICO  ============="<<endl;
				cout<<"=============                      ============="<<endl;
				cout<<"=============                      ============="<<endl;
				cout<<"=============        LOGIN         ============="<<endl;
				cout<<"=============                      ============="<<endl;
				cout<<"=============                      ============="<<endl;
				cout<<"================================================"<<endl;
				cout<<""<<endl;
				cout<<""<<endl;
}
