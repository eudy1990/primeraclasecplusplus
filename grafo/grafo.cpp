#include <iostream>
using namespace std;

//variable globales
char opc;
char volver;
char valor;


//funciones

//declarar el nodo mas dos punteros 
struct Nodo {
	int numero;//nombre del vertice o nodo
	struct Nodo *siguiente;
	struct Arista *adyancente;//puntero hacia la primera arista del nodo
};

//instanciamos el nodo y creamos una variable puntero
typedef struct Nodo *TNodo;
TNodo puntero;//puntero cabeza 

struct Arista{
	struct Nodo *destino;//puntero al nodo de llegada
	struct Nodo *siguiente;
};

//instanciamos arista
typedef struct Arista *Tarista;

void insertar_nodo(){
	//instanciamos el nodo y declaramos un nuevo puntero
	TNodo t,nuevo = new struct Nodo;
	cout<<"Ingrese un numero para el nodo: "<<endl;
	//capturar el nuevo numero
	cin>>nuevo->numero;
	nuevo->siguiente=NULL;
	nuevo->adyancente=NULL;
	
	//validamos si el puntero esta vacio
	if(puntero == NULL){
		puntero = nuevo;
		cout<<"Nodo ingresado"<<endl;
	}else{
		//Si no debe recorrer este puntero para preguntar por la variable siguiente
		//que esta vacia
		t= puntero;
		while(t->siguiente != NULL){
			t= t->siguiente;
		}
		
		t->siguiente=nuevo;
		cout<<"Nodo ingresado"<<endl;
	}
}
	
//funcion para agregar la Arista
void agregar_arista(TNodo &aux,TNodo &aux2, Tarista &nuevo ){
	//con el puntero de l
	Tarista q;
	if(aux->adyancente == NULL){
		aux->adyancente = nuevo;
		nuevo->destino=aux2;
	}else{
		q=aux->adyancente;
		while(q->siguiente != NULL){
			q=q->siguiente;
		}
		
		nuevo->destino = aux2;
		q->siguiente = nuevo;
		cout<<"Arista ingresada"<<endl;
	}
}
	
void insertar_arista(){
	int ini,fin;
	Tarista nuevo = new struct arista;
	TNodo aux,aux2;
	if(puntero ==NULL){
		cout<<"No hay grafos"<<endl;
		return;
	}
	
	nuevo->siguiente = NULL;
	cout<<"ingrese el nodo de inicio "<<endl;
	cin>>ini;
	cout<<"ingrese el nodo de fin "<<endl;
	cin>>fin;
		
	aux = puntero;
	aux2 = puntero;
	
	while(aux2 != NULL){
		if(aux2->numero == fin ){
			break;
		}
		aux2=aux2->siguiente;
	}
	
	while(aux != NULL){
		if(aux->numero == ini ){
			agregar_arista(aux,aux2,nuevo);
			break;
		}
		aux=aux->siguiente;
	}
	
}

int main(int argc, char *argv[]) {
	
	return 0;
}

